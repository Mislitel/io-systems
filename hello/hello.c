// Заголовочные файлы ядра
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

// Метаданные модуля ядра
MODULE_LICENSE("WTFPL");
MODULE_AUTHOR("Dmitrii Medvedev");
MODULE_DESCRIPTION("A simple example Linux module.");
MODULE_VERSION("0.1");

// Функция инициализации модуля ядра (вызывается при insmod/modprobe)
static int __init hello_init(void) {
	printk(KERN_INFO "Hello, World!\n");
	return 0;
}

// Функция очистки модуля ядра (вызывается при rmmod)
static void __exit hello_exit(void) {
	printk(KERN_INFO "Goodbye, World!\n");
}

// Регистрация определенных выше функций
module_init(hello_init);
module_exit(hello_exit);

